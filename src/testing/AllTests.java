package testing;

import java.io.IOException;

import org.apache.log4j.Level;

import app_kvServer.KVServer;
import junit.framework.Test;
import junit.framework.TestSuite;
import logger.LogSetup;

import app_kvEcs.ECSClient;
import app_kvEcs.ECSFunc;

public class AllTests {

	//Global variables for actions that need to be performed for ECS
	public static ECSClient EC;
	public static ECSFunc EF;

	static {
		try {
			new LogSetup("logs/testing/test.log", Level.ERROR);
			
			EC = new ECSClient();

			String args = "test_ecs.config";
			ECSClient.parse_configuartion_file(EC, args);
			EF = new ECSFunc(true, "127.0.0.1", 40501, null, -1);

			
			EC.handle_command("initService 4 1024 FIFO", EF);
			EC.handle_command("start", EF);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Set up Error");
		}
	}
	
	
	public static Test suite() {
		TestSuite clientSuite = new TestSuite("Basic Storage ServerTest-Suite");
		clientSuite.addTestSuite(ConnectionTest.class);
		clientSuite.addTestSuite(InteractionTest.class); 
		clientSuite.addTestSuite(AdditionalTest.class); 
		return clientSuite;
	}
	
}
