package app_kvEcs;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import client.KVStore;
import common.messages.KVMessage;
import common.messages.TextMessage;
import common.messages.KVMessage.StatusType;
import common.messages.TextMessage.MessageParameters;
import logger.LogSetup;

import java.io.BufferedReader;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.lang.ProcessBuilder;

import java.net.Socket;
import java.io.InputStream;
import java.io.OutputStream;











public class ECSClient {

	// Logging events:
	static Logger logger = Logger.getRootLogger();
	
	// ECS CLI parameters:
	private boolean stop = false;
	private static final String PROMPT = "ECS-Client> ";
	private BufferedReader stdin;	
	
	// List of available server entries:
	static List<ServerEntry> available_servers = new ArrayList<ServerEntry>();
	
	// Used to store the current master server if any:
	private final String MASTER_SERVER_FILE = "./master_server.config";
	private ServerEntry master_server = null;
	
	// Used for KVServers to alert failures to:
	ServerEntry failure_server = null;
	
	
	/**
	 * 
	 * Once the configuration file is read, we loop here and process user commands
	 * 
	 */
	public void run() {
		
		// Used to communicate with KVServers and provide an API:
		//
		ECSFunc ecs_api = null;
		
		// We pass the master server read, if any:
		//
		if(master_server == null)
			ecs_api = new ECSFunc(false, null, -1, failure_server.hostname, failure_server.port);
		else
			ecs_api = new ECSFunc(true, master_server.hostname, master_server.port, failure_server.hostname, failure_server.port);
		
		
		

		// Used to receive and act upon failure alerts:
		//
		FailureServer f_server = new FailureServer(failure_server.hostname, failure_server.port, ecs_api, available_servers);
		f_server.setDaemon(true);
        f_server.start();
       
    	// Wait 2 seconds to give time for the failure server to be able to start accepting connections:
			//
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		
		
		
		
		
		// While the user doesn't enter "quit"
		while(!stop){
			stdin = new BufferedReader(new InputStreamReader(System.in));
			System.out.print("\n");
			System.out.print(PROMPT);
			
			
			
			// Read a command:
			try {
				String cmdLine = stdin.readLine();

				// Get control over ECS (vs. the failure server), before acting
				// on KVServers:
				//
				try {
					ecs_api.acquire_ECS_control();
					this.handle_command(cmdLine, ecs_api);
				} finally {
					ecs_api.release_ECS_control(); // Let Failure Server acquire ECS control
				}
				
				
			} catch (IOException e) {
				printError("CLI does not respond - Application terminated ");
			}
			
			
		}
	}
	
	
	
	
	
	
    /**
     * Main entry point for the echo server application. 
	 * 
	 */
    public static void main(String[] args) {
    	
    	// Check if user input the configuration file:
    	if(args.length != 1){
    		System.out.println("Error! Incorrect number of arguments.");
    		System.out.println("Correct usage is:\n"
    							+ "ECS   <path_to_config_file>");
    		System.out.println("Example usage: java -jar ms2-ecs.jar   ecs.config");
    		System.exit(0);
    	}
    		
    	// ECS Client object, used  to fill in server list
    	ECSClient cli = new ECSClient();
    	
    	
    	// Attempt to parse the configuration file and start the logger:
    	try {
    		parse_configuartion_file(cli, args[0]);
    		parse_master_file(cli, cli.MASTER_SERVER_FILE);
			new LogSetup("logs/ecs.log", Level.ALL);
			logger.info("Starting the ECS Server");
		}
    	catch (IOException e) {
			System.out.println("Error! Unable to initialize logger!");
			e.printStackTrace();
			System.exit(1);
		}
    	
    	// Successfully read configuration file.
    	cli.run();
    }
    
    
    
    
    
    
    
    

	/**
	 * 
	 * Takes a command and acts upon its arguments
	 * 
	 * @param cmdLine: Command entered in the CLI
	 */
	public void handle_command(String cmdLine, ECSFunc ecs_api) {
		String[] tokens = cmdLine.split("\\s+");

		
		// Launch available KVServers from the list of servers though SSH calls:
		if(tokens[0].equals("initService")) {	
			
			if(tokens.length != 4){
				System.out.println("Expected usage:\n"+"initService <numberOfNodes> <cacheSize> <replacementStrategy>");
				return;
			}
			
			
			// Check for a valid cache size:
			int cachesize = 0;
			try{
				cachesize = Integer.parseInt(tokens[2]);
			}catch(NumberFormatException e){
				 System.out.println("Could not read cachesize " + cachesize + ".");
				 return;
			}
			if(cachesize <= 0){
				 System.out.println("Cache size " + cachesize + " is less than or equal to 0.");
				 return;
			}

			
			// Check for a valid replacement strategy:
			if(!tokens[3].equals("LRU") && !tokens[3].equals("LFU") && !tokens[3].equals("FIFO") ){
				System.out.println("Expected usage:\n"+"initService <numberOfNodes> <cacheSize> <replacementStrategy>");
				System.out.println("Where <replacementStrategy> is one of {LRU, LFU, FIFO}.");
				return;
			}
			
			// Use api to launch KVServer through SSH:
			int num_servers_added = ecs_api.initService(tokens[1], tokens[2], tokens[3], available_servers);
			logger.info("\n\n\nA total of " + num_servers_added + " servers have been added successfully by initService.");

		} 
		
		
		

		
		
		
		// All started KVServers now accept requests:
		else if(tokens[0].equals("start")) {	
			int num_servers_affected = ecs_api.set_service_status(true);
			logger.info("A total of " + num_servers_affected + " servers have been started successfully.");
		}
		// All started KVServers now stop accepting requests:
		else if(tokens[0].equals("stop")) {	
			int num_servers_affected = ecs_api.set_service_status(false);
			logger.info("A total of " + num_servers_affected + " servers have been stopped successfully.");
		} 
		
		// All started KVServers now exit:
		else if(tokens[0].equals("shutdown")) {	
			int num_servers_shutdown = ecs_api.shutdown_all_servers(available_servers);
			logger.info("A total of " + num_servers_shutdown + " servers have been shutdown successfully.");
		} 
		
		
		
		
		
		
		
		// Launch one available KVServer through SSH calls:
		else if(tokens[0].equals("addNode")) {
			
			// Check number of arguments:
			if(tokens.length != 3){
				System.out.println("Expected usage: addNode <cacheSize> <replacementStrategy>");
				return;
			}	
				
			
			// Check for a valid cache size:
			int cachesize = 0;
			try{
				cachesize = Integer.parseInt(tokens[1]);
			}catch(NumberFormatException e){
				 System.out.println("Could not read cachesize " + cachesize + ".");
				 return;
			}
			if(cachesize <= 0){
				 System.out.println("Cache size " + cachesize + " is less than or equal to 0.");
				 return;
			}

			
			// Check for a valid replacement strategy:
			if(!tokens[2].equals("LRU") && !tokens[2].equals("LFU") && !tokens[2].equals("FIFO") ){
				System.out.println("Expected usage: addNode <port number> <cacheSize> <replacementStrategy>");
				System.out.println("Where <replacementStrategy> is one of {LRU, LFU, FIFO}.");
				return;
			}
			
			// Use api to launch KVServer through SSH:
			if(ecs_api.addNode(tokens[1], tokens[2], available_servers)==-1){
				System.out.println("addNode unsuccessful");
			}
			
		} 
		
		
		
		
		
		
		
		
		
		// remove one available KVServer through SSH calls:
		else if(tokens[0].equals("removeNode")) {	
			
			if(ecs_api.removeNode(available_servers)==-1){
				
				System.out.println("removeNode unsuccessful");
				
			}
		
		} 
				
		
		
		// Exit CLI:
		else if(tokens[0].equals("quit")) {	
			int num_servers_shutdown = ecs_api.shutdown_all_servers(available_servers);
			logger.info("A total of " + num_servers_shutdown + " servers have been shutdown successfully.");
			stop = true;
		} 
		
		
		
		// Change log level
		else if(tokens[0].equals("logLevel")) {
			if(tokens.length == 2) {
				String level = setLevel(tokens[1]);
				if(level.equals(LogSetup.UNKNOWN_LEVEL)) {
					printError("No valid log level!");
					printPossibleLogLevels();
				} else {
					System.out.println(PROMPT + 
							"Log level changed to level " + level);
				}
			} else {
				printError("Invalid number of parameters!");
			}
			
		} 
		
		
		
		// Print all commands
		else if(tokens[0].equals("help")) {
			printHelp();
		} 
		
		
		// Default to unknown command:
		else {
			printError("Unknown command");
			printHelp();
		}
		
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
	 * Helper function to print out all commands.
	 */
	private void printHelp() {
		StringBuilder sb = new StringBuilder();
		sb.append(PROMPT).append("ECS CLIENT HELP (Usage):\n");
		sb.append(PROMPT);
		sb.append("::::::::::::::::::::::::::::::::");
		sb.append("::::::::::::::::::::::::::::::::\n");
		sb.append(PROMPT).append("initService <numberOfNodes> <cacheSize> <replacementStrategy>");
		sb.append("\t Randomly choose <numberOfNodes> servers from the available machines and start the KVServer by issuing an SSH call to the respective machine.  \n");
		sb.append(PROMPT).append("start");
		sb.append("\t Starts the storage service on servers spawned through SSH.\n");
		sb.append(PROMPT).append("stop");
		sb.append("\t Stops the service.\n");
		sb.append(PROMPT).append("shutDown");
		sb.append("\t Stops all server instances and exits the remote processes.\n");
		sb.append(PROMPT).append("addNode <cacheSize> <replacementStrategy>");
		sb.append("\t Create a new KVServer with the specified cache size and replacement strategy and add it to the storage service at an arbitrary position.\n");
		sb.append(PROMPT).append("removeNode");
		sb.append("\t Remove a server from the storage service at an arbitrary position.\n");
		
		
		sb.append(PROMPT).append("logLevel");
		sb.append("\t\t\t changes the logLevel \n");
		sb.append(PROMPT).append("\t\t\t\t ");
		sb.append("ALL | DEBUG | INFO | WARN | ERROR | FATAL | OFF \n");
		
		sb.append(PROMPT).append("quit ");
		sb.append("\t\t\t exits the program");
		System.out.println(sb.toString());
	}
	
	
	/**
	 * Helper function to print out all log levels.
	 */
	private void printPossibleLogLevels() {
		System.out.println(PROMPT 
				+ "Possible log levels are:");
		System.out.println(PROMPT 
				+ "ALL | DEBUG | INFO | WARN | ERROR | FATAL | OFF");
	}

	
	/**
	 * 	
	 * Helper function to set the logger's log level.
	 * 
	 * @param levelString: Desired log level
	 * @return: Level set
	 */
	private String setLevel(String levelString) {
		
		if(levelString.equals(Level.ALL.toString())) {
			logger.setLevel(Level.ALL);
			return Level.ALL.toString();
		} else if(levelString.equals(Level.DEBUG.toString())) {
			logger.setLevel(Level.DEBUG);
			return Level.DEBUG.toString();
		} else if(levelString.equals(Level.INFO.toString())) {
			logger.setLevel(Level.INFO);
			return Level.INFO.toString();
		} else if(levelString.equals(Level.WARN.toString())) {
			logger.setLevel(Level.WARN);
			return Level.WARN.toString();
		} else if(levelString.equals(Level.ERROR.toString())) {
			logger.setLevel(Level.ERROR);
			return Level.ERROR.toString();
		} else if(levelString.equals(Level.FATAL.toString())) {
			logger.setLevel(Level.FATAL);
			return Level.FATAL.toString();
		} else if(levelString.equals(Level.OFF.toString())) {
			logger.setLevel(Level.OFF);
			return Level.OFF.toString();
		} else {
			return LogSetup.UNKNOWN_LEVEL;
		}
	}
	
    
    
	/**
	 * Helper function to print an error.
	 * 
	 * @param error: Error message
	 */
	private void printError(String error){
		System.out.println(PROMPT + "Error! " +  error);
	}
	
	
    
    /**
     * 
     * Reads the ECS configuration file containing all server hostnames and ports.
     * Adds every entry to a local list.
     * 
     * Also checks for validity of the list and exits if necessary.
     * 
     * @param filename: Path of the configuration file
     */
    public static void parse_configuartion_file(ECSClient cli, String filePath){
    	
    	File file = new File(filePath);
    	
    	Scanner config_file = null;
    	try{
    		config_file = new Scanner(file);
    	}
    	catch(FileNotFoundException e){
    		System.out.println("Unable to find configuration file \"" + filePath + "\".");
    		System.out.println("Exiting...");
			System.exit(1);
    	}

    	
    	
    	// IF we find the file:
    	if(config_file != null){
    		
    		// Read each line in the file:
	    	while(config_file.hasNext()) {
	    	    String current_line = config_file.nextLine();
	    	    String[] server_info = current_line.split(" ");
	    	    
	    	    
	    	    // KVServer entry:
	    	    if(server_info[0].equals("KVServer")){
	    	    	
		    	    // Check overall format:
		    	    if (server_info.length != 4){
		    	    	System.out.println("Line in configuration file has an incorrect number of arguments: \"" + current_line  +"\".");
		    	    	System.out.println("Expected format: KVServer <hostname> <port> <path_to_jar>");
		    	    	System.out.println("\n\nExiting....");
		    			System.exit(0);
		    	    }
		    	    
		    	    
		    	    // Catch port in wrong position
		    	    int current_port = 0;
		    	    try{
		    	    	current_port = Integer.parseInt(server_info[2]);
		    	    }
		    	    catch(NumberFormatException e){
		    	    	System.out.println("Line in configuration file has invalid port: \"" + current_line  +"\".");
		    	    	System.out.println("Attempted to read port number \""+ server_info[2] + "\".");
		    	    	System.out.println("Expected format: KVServer <hostname> <port> <path_to_jar>");
		    	    	System.out.println("\n\nExiting....");
		    			System.exit(0);
	    	    	}
		    	    
		    	    // Add server entry to available server list:
		    	    available_servers.add(new ServerEntry(server_info[1], current_port, server_info[3]));
		    	    
	    	    }
	    	    
	    	    
	    	    
	    	    
	    	    
	    	    // FailureServer entry:
	    	    else if(server_info[0].equals("FailureServer")){
	    	    	if(cli.failure_server != null){
		    	    	System.out.println("There should only be one failure server.");
		    	    	System.out.println("\n\nExiting....");
		    			System.exit(0);
	    	    	}
	    	    	
	    	    	// Check overall format:
		    	    if (server_info.length != 3){
		    	    	System.out.println("Line in configuration file has an incorrect number of arguments: \"" + current_line  +"\".");
		    	    	System.out.println("Expected format: FailureServer <hostname> <port>");
		    	    	System.out.println("\n\nExiting....");
		    			System.exit(0);
		    	    }
		    	    
		    	    // Catch port in wrong position
		    	    int current_port = 0;
		    	    try{
		    	    	current_port = Integer.parseInt(server_info[2]);
		    	    }
		    	    catch(NumberFormatException e){
		    	    	System.out.println("Line in configuration file has invalid port: \"" + current_line  +"\".");
		    	    	System.out.println("Attempted to read port number \""+ server_info[2] + "\".");
		    	    	System.out.println("Expected format: FailureServer <hostname> <port>");
		    	    	System.out.println("\n\nExiting....");
		    			System.exit(0);
	    	    	}
		    	
		    	    
		    	    
		    	    // Add the failure server:
		    	    cli.failure_server = new ServerEntry(server_info[1], current_port, null);
	    	    	
	    	    }
	    	    
	    	    
	    	    
	    	    else{
	    	    	System.out.println("Expected format: KVServer <hostname> <port> <path_to_jar>");
	    	    	System.out.println("Expected format: FailureServer <hostname> <port>");
	    	    	System.out.println("\n\nExiting....");
	    			System.exit(0);
	    	    }
	    	    
	    	}
	    	
    	}
    	
    	
    	
    	
    	
    	// Ensure we have a failure server...:
    	if(cli.failure_server == null){
    		System.out.println("There is no failure server.");
	    	System.out.println("Expected format: FailureServer <hostname> <port>");
	    	System.out.println("\n\nExiting....");
			System.exit(0);
    	}

    }
    
    
    
    
    
    /**
     * 
     * Reads the master server configuration file for a past created master server.
     * 
     * Also checks for validity of the list and exits if necessary.
     * 
     * @param filename: Path of the master server configuration file
     */
    public static void parse_master_file(ECSClient cli, String filePath){
    	
    	File file = new File(filePath);
    	
    	Scanner config_file = null;
    	try{
    		config_file = new Scanner(file);
    	}
    	catch(FileNotFoundException e){
    		System.out.println("Unable to find configuration file \"" + filePath + "\".");
    		System.out.println("Exiting...");
			System.exit(1);
    	}

    	
    	
    	// IF we find the file:
    	if(config_file != null){
    		
    		int num_lines = 0;
    		// Read each line in the file:
	    	while(config_file.hasNext()) {
	    		
	    		// There can only be one master server:
	    		num_lines++;
	    		if(num_lines > 1){
	    	    	System.out.println("There can only be one master server (or none).");
	    	    	System.exit(0);
	    		}
	    		
	    		
	    	    String current_line = config_file.nextLine();
	    	    String[] server_info = current_line.split(" ");
	    	    
	    	    
	    	    
	    	    // Check overall format:
	    	    if (server_info.length != 2){
	    	    	System.out.println("Line in master server configuration file has an incorrect number of arguments: \"" + current_line  +"\".");
	    	    	System.out.println("Expected format: <hostname> <port>");
	    	    	System.out.println("\n\nExiting....");
	    			System.exit(0);
	    	    }
	    	    
	    	    
	    	    // Catch port in wrong position
	    	    int current_port = 0;
	    	    try{
	    	    	current_port = Integer.parseInt(server_info[1]);
	    	    }
	    	    catch(NumberFormatException e){
	    	    	System.out.println("Line in configuration file has invalid port: \"" + current_line  +"\".");
	    	    	System.out.println("Attempted to read port number \""+ server_info[1] + "\".");
	    	    	System.out.println("Expected format: <hostname> <port>");
	    	    	System.out.println("\n\nExiting....");
	    			System.exit(0);
    	    	}
	    	    
	    	    // Add server entry to available server list:
	    	    cli.master_server = new ServerEntry(server_info[0], current_port, null);
	    	    
	    	}
	    	
    	}
    	
    	
    	// If we read a master server:
    	//
    	if(cli.master_server != null){
    		
	    	// If we are here, we successfully read the master server:
	    	//
	    	// Now we check if the master server exists in the original pool:
	    	//
	    	Boolean found_master_server = false;
	    	for(ServerEntry server : available_servers){
	    		if(server.hostname.equals(cli.master_server.hostname) && (server.port == cli.master_server.port)){
	    			found_master_server = true;
	    			break;
	    		}
	    	}
	    	
	    	if(!found_master_server){
		    	System.out.println("Could not find master server (" + cli.master_server.hostname + "," + cli.master_server.port + ") in the pool of available servers (ecs.config).");
	    		System.exit(0);
	    	}
    	}
    	
    	
    }
}
