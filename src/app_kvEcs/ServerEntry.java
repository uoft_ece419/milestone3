package app_kvEcs;


/**
 * Used to store available server entries from the configuration file.
 */
public class ServerEntry{
	public String hostname;
	public String jar_path;
	public int port;
	
	
	public ServerEntry(String _host, int _port, String _jar_path){
		this.hostname = _host;
		this.port = _port;
		this.jar_path = _jar_path;
	}
}