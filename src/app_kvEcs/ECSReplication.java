package app_kvEcs;

import java.util.List;

import org.apache.log4j.Logger;

import app_kvEcs.ECSFunc.ServerConnection;
import common.messages.ECSMessage;
import common.messages.MetadataEntry;
import common.messages.ECSMessage.ECS_Status;

/*
 * Functionality for the replication of KVServer data
 * 
 */
public class ECSReplication {

	static Logger logger = Logger.getRootLogger();

	
	
	
	/*
	 * Used in initService only.
	 * 
	 * 
	 * *** ECSFunc already guarantees this for us:
	 * (NOTE)  : Assumes that all KVServers already have their coordinator key range data and correct metadata.
	 * (NOTE2) : Assumes all servers are WRITE_LOCKed.
	 * (NOTE3) : Assumes that any keys that the server is no longer responsible for will be deleted by some other functionality (should be done by WRITE_UNLOCK).
	 * 
	 * 
	 * 
	 * Performs the following:
	 * 
	 * 			for each KVServer:
	 * 				-> send coordinator data to next two servers (replicas)
	 * 
	 * @return: True: No server failed during replicate_data ---> successful
	 * 			False: A server failed, we return early.
	 */
	Boolean replicate_data(ECSFunc ecs_api){
		
		logger.info("\n\nReplicating data in initServce().");
		
		// For each server:
		//
		Boolean server_died = false;
		
		for (ServerConnection cur_server : ecs_api.server_connections) {
		
			if(!server_died){
				
				// Get the next two servers in the ring (if they exist):
				//
				ServerConnection first_replica = get_first_successor(ecs_api, cur_server);
				ServerConnection second_replica = get_second_successor(ecs_api, cur_server);
	
				
				
				
				/*
				 *  Write lock the current server:
				 * 
				 */
				ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, cur_server.metadata.hostname, null, null, null, -1, null, -1);
		        ECS_Status response_type = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);
				
				// Check response type:
				if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
					logger.info("Received a write lock ACK from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
				else
					server_died = true;
				
				
				
				
				/*
				 * Send the current server's coordinator data to the replicas (if they exist)
				 * 
				 */
				if(first_replica != null){
					
					encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, first_replica.metadata.hostname, cur_server.metadata.prev_hash_range, cur_server.metadata.hash, first_replica.metadata.port, null, -1);
			        response_type = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);
					
			        // Check response type:
					if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE){
							logger.info("Received a movedata failure ACK from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
							server_died = true;
					}
					else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
							logger.info("Received a movedata success ACK from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
					else{
						server_died = true;
					}
				}
				
				if(second_replica != null){
					
					encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, second_replica.metadata.hostname, cur_server.metadata.prev_hash_range, cur_server.metadata.hash, second_replica.metadata.port, null, -1);
			        response_type = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);
					
			        // Check response type:
					if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE){
							logger.info("Received a movedata failure ACK from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
							server_died = true;
					}
					else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
							logger.info("Received a movedata success ACK from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
					else{
						server_died = true;
					}
				}
				
			}
			
			
		}
		
		
		
		/*
		 * Unlock all servers:
		 */
		for (ServerConnection cur_server : ecs_api.server_connections) {
			
			
			// Unlock the master server:
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, cur_server.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received a write unlock ACK from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");
			
		}
			
		
		return !server_died;
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * Used only by addNode().
	 * 
	 * 
	 * (NOTE)  : Assumes that all KVServers already have their coordinator key range data  ----> ECSFunc already guarantees this for us.
	 * (NOTE2) : Assumes that any keys that the server is no longer responsible for will be deleted by some other functionality (should be done by WRITE_UNLOCK).
	 *
	 *
	 *
	 *
	 * Performs:
	 *
	 *		For new server:
	 *			- Get coordinator data from previous two servers to the current server (if they even exist) 
	 *				----> If any of the previous two servers die, we try the successor for the replica of the data we're looking for.			
	 *
	 *			- Remove data that is no longer responsible for the next three servers in the ring and the previous two (if they exist).
	 *			
	 *
	 *
	 * @param ecs_api: The ECS main api
	 * @param new_node: The newly added node.
	 * @param metadata_entries: Sorted metadata of all servers.
	 * @param failure_hostname: Hostname of the failure server.
	 * @param failure_port: Port of the failure server.
	 * 
	 */
	void single_node_replicate_data(ECSFunc ecs_api, ServerConnection new_node, List<MetadataEntry> metadata_entries, String failure_hostname, int failure_port){

		
		/* 
		 * All servers in the ring other than the new node we will have to (potentially) contact.
		 */
		ServerConnection previous_node = get_first_predecessor(ecs_api, new_node);
		ServerConnection second_previous_node = get_second_predecessor(ecs_api, new_node);
		ServerConnection first_successor = get_first_successor(ecs_api, new_node);
		ServerConnection second_successor = get_second_successor(ecs_api, new_node);
		ServerConnection third_successor = get_third_successor(ecs_api, new_node);

		
		
		

		/*
		 * Set write lock on all servers
		 */
		if(previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
		}
		if(second_previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, second_previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
		}
		if(first_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, first_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
		}
		if(second_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, second_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ second_successor.metadata.hostname +","+ second_successor.metadata.port +").");
		}
		if(third_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, third_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(third_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ third_successor.metadata.hostname +","+ third_successor.metadata.port +").");
		}
		
		
		
		
		
		
		
		
		
		
		// Send a metadata update to all storage servers(the rest of the servers other than the newly instantiated one)
		//
		for (ServerConnection cur_server : ecs_api.server_connections) {
			
			// Create message to send:
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, cur_server.metadata.hostname, null, null, null, -1, failure_hostname, failure_port);
			ECS_Status response_type = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);
				
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.INITIALIZE_ACK)
				logger.info("Received initialization of metadata response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");		
		}
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*
		 * Get coordinator data to newly added node from previous two nodes in ring:
		 * 
		 */
		
		
		logger.info("\n\n Getting coordinator data of previous two servers. \n\n");
		
		// IF the previous node exists, get its coordinator data:
		//
		if(previous_node != null){
			
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, new_node.metadata.hostname, previous_node.metadata.prev_hash_range, previous_node.metadata.hash, new_node.metadata.port, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(previous_node, encapsulated_message);
			
	        // Check response type:
			if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
					logger.info("Received a movedata failure ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
			else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
					logger.info("Received a movedata success ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
			
			
			// Previous node died, try successor instead:
			else{
				
				encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, new_node.metadata.hostname, previous_node.metadata.prev_hash_range, previous_node.metadata.hash, new_node.metadata.port, null, -1);
		        response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
				
		        
		        // Check response type:
		        Boolean move_success = false;
				if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
						logger.info("Received a movedata failure ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
				else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
						logger.info("Received a movedata success ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
						
			}
		}
		
		
		
		// IF the second previous node exists, get its coordinator data:
		//
		if(second_previous_node != null){
			
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, new_node.metadata.hostname, second_previous_node.metadata.prev_hash_range, second_previous_node.metadata.hash, new_node.metadata.port, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_previous_node, encapsulated_message);
			
	        // Check response type:
			if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
					logger.info("Received a movedata failure ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
			else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
					logger.info("Received a movedata success ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
			
			

			// Second previous node died, try successor instead:
			else{
				
				encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, new_node.metadata.hostname, previous_node.metadata.prev_hash_range, previous_node.metadata.hash, new_node.metadata.port, null, -1);
		        response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
				
		        
		        // Check response type:
		        Boolean move_success = false;
				if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
						logger.info("Received a movedata failure ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
				else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
						logger.info("Received a movedata success ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
						
			}
		}
	
		
		
		
		
		
		

		
	
		/*
		 * Unlock all the servers originally acted on.
		 *
		 */
		if(previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
		}
		if(second_previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, second_previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
		}
		if(first_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, first_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
		}
		if(second_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, second_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ second_successor.metadata.hostname +","+ second_successor.metadata.port +").");
		}
		if(third_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, third_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(third_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ third_successor.metadata.hostname +","+ third_successor.metadata.port +").");
		}
		
		
	
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * Used only by removeNode().
	 * 
	 * 
	 * (NOTE)  : Assumes that all KVServers already have their coordinator key range data  ----> ECSFunc already guarantees this for us.
	 * (NOTE2) : Assumes that any keys that the server is no longer responsible for will be deleted by some other functionality (should be done by WRITE_UNLOCK).
	 *
	 *
	 *
	 *
	 * Performs (Check this on paper if you're confused):
	 *
	 *		For server to be deleted:
	 *			- Send coordinator data of successor to second successor and third successor (if they even exist).
	 *			- Send coordinator data of predecessor to our second successor. (If they both exist).
	 *			- Send coordinator data of second predecessor to our successor. (If they both exist).
	 *			
	 *
	 *
	 *
	 * @param ecs_api: The ECS main api
	 * @param new_node: The newly added node.
	 * @param metadata_entries: Sorted metadata of all servers.
	 * @param failure_hostname: Hostname of the failure server.
	 * @param failure_port: Port of the failure server.
	 * 
	 * @param previous_node: First predecessor in the ring of the node to be removed.
	 * @param second_previous_node: Second predecessor in the ring of the node to be removed.
	 * @param first_successor: Next server node in the ring.
	 * @param second_successor: Next, next server node in the ring.
	 * @param third_successor: Next, next, next server node in the ring.
	 * 
	 */
	void single_node_remove_replicated_data(ECSFunc ecs_api, ServerConnection node_to_remove, List<MetadataEntry> metadata_entries, String failure_hostname, int failure_port,
							
											ServerConnection previous_node, ServerConnection second_previous_node, ServerConnection first_successor, ServerConnection second_successor,
											ServerConnection third_successor){
		
		
		
		logger.info("\n\n\nMaintaining replication invariant due to removal of a server node.\n\n\n");	
	
		
		

		/*
		 * Set write lock on all servers
		 */
		if(previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
		}
		if(second_previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, second_previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
		}
		if(first_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, first_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
		}
		if(second_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, second_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ second_successor.metadata.hostname +","+ second_successor.metadata.port +").");
		}
		if(third_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_LOCK, null, third_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(third_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_LOCK_ACK)
				logger.info("Received write lock ACK from ("+ third_successor.metadata.hostname +","+ third_successor.metadata.port +").");
		}
		
		
		
		
		
		
		
		
		
		
		// Send a metadata update to all storage servers(the rest of the servers other than the newly instantiated one)
		//
		for (ServerConnection cur_server : ecs_api.server_connections) {
			
			// Create message to send:
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.INITIALIZE_METADATA, metadata_entries, cur_server.metadata.hostname, null, null, null, -1, failure_hostname, failure_port);
			ECS_Status response_type = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);
				
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.INITIALIZE_ACK)
				logger.info("Received initialization of metadata response from ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +").");		
		}
		
		
		
		
		
		
		/*
		 * Send coordinator data of our successor to our second successor and third successor (if they even exist):
		 * 
		 */
		if(first_successor != null){
			
			
			if(second_successor != null){
				
				ECSMessage encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, second_successor.metadata.hostname, first_successor.metadata.prev_hash_range, first_successor.metadata.hash, second_successor.metadata.port, null, -1);
		        ECS_Status response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
				
		        // Check response type:
				if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
						logger.info("Received a movedata failure ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
				else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
						logger.info("Received a movedata success ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
			}
			
			
			if(third_successor != null){
			
				ECSMessage encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, third_successor.metadata.hostname, first_successor.metadata.prev_hash_range, first_successor.metadata.hash, third_successor.metadata.port, null, -1);
		        ECS_Status response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
				
		        // Check response type:
				if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
						logger.info("Received a movedata failure ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
				else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
						logger.info("Received a movedata success ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
			}
			
		}
		
		
		
		
		
		
		
		
		
		/*
		 * Send coordinator data of our predecessor to our second successor, if they exist:
		 * 
		 */
		if(second_successor != null && previous_node != null){
			
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, second_successor.metadata.hostname, previous_node.metadata.prev_hash_range, previous_node.metadata.hash, second_successor.metadata.port, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(previous_node, encapsulated_message);
			
	        // Check response type:
			if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
					logger.info("Received a movedata failure ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
			else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
					logger.info("Received a movedata success ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
			
		}
		
		
		
	
		
		

		/*
		 * Send coordinator data of our second predecessor to our successor, if they exist:
		 * 
		 */
		if(first_successor != null && second_previous_node != null){
			
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.MOVE_DATA, null, null, first_successor.metadata.hostname, second_previous_node.metadata.prev_hash_range, second_previous_node.metadata.hash, first_successor.metadata.port, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_previous_node, encapsulated_message);
			
	        // Check response type:
			if(response_type != null && response_type == ECS_Status.MOVE_DATA_FAILURE)
					logger.info("Received a movedata failure ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
			else if(response_type != null && response_type == ECS_Status.MOVE_DATA_SUCCESS)
					logger.info("Received a movedata success ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
			
		}
		
		
		
		
		
		

		/*
		 * Unlock all the servers originally acted on.
		 *
		 */
		if(previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ previous_node.metadata.hostname +","+ previous_node.metadata.port +").");
		}
		if(second_previous_node != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, second_previous_node.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_previous_node, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ second_previous_node.metadata.hostname +","+ second_previous_node.metadata.port +").");
		}
		if(first_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, first_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(first_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ first_successor.metadata.hostname +","+ first_successor.metadata.port +").");
		}
		if(second_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, second_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(second_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ second_successor.metadata.hostname +","+ second_successor.metadata.port +").");
		}
		if(third_successor != null ){
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.WRITE_UNLOCK, null, third_successor.metadata.hostname, null, null, null, -1, null, -1);
	        ECS_Status response_type = ecs_api.forwarding_message_to_server(third_successor, encapsulated_message);
			
			// Check response type:
			if(response_type != null && response_type == ECS_Status.WRITE_UNLOCK_ACK)
				logger.info("Received write unlock ACK from ("+ third_successor.metadata.hostname +","+ third_successor.metadata.port +").");
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/* 
	 * 
	 * HELPER FUNCTIONS:
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	

	/**
	 * Helper function
	 * 
	 * @param ecs_api: ECS main api
	 * @param connection: server to act on
	 * @return: second predecessor
	 */
	ServerConnection get_second_predecessor(ECSFunc ecs_api, ServerConnection connection){

		// There are no second predecessors:
		if(ecs_api.server_connections.size() < 3){
			return null;
		}

		// Index of current server:
		int cur_server_index = get_index_from_server_connections(ecs_api, connection);
		if(cur_server_index==-1){
			logger.error("cannot find the current server"); 
			return null;
		}
		
		ServerConnection predecessor_server = null;

		
		
		// We are not the first two servers:
		if(cur_server_index > 1){
			predecessor_server = ecs_api.server_connections.get(cur_server_index-2);
		}
		
		// We are one of the first two:
		else{
			
			// Second server:
			if(cur_server_index == 1)
				predecessor_server = ecs_api.server_connections.get(ecs_api.server_connections.size()-1);
			// First server:
			else 
				predecessor_server = ecs_api.server_connections.get(ecs_api.server_connections.size()-2);
		}
		
		
		return predecessor_server;
	}
	
	
	
	
	
	
	/**
	 * Helper function
	 * 
	 * @param ecs_api: ECS main api
	 * @param connection: server to act on
	 * @return: first predecessor
	 */
	ServerConnection get_first_predecessor(ECSFunc ecs_api, ServerConnection connection){

		// There are no predecessors:
		if(ecs_api.server_connections.size() < 2){
			return null;
		}

		// Index of current server:
		int cur_server_index = get_index_from_server_connections(ecs_api, connection);
		if(cur_server_index==-1){
			logger.error("cannot find the current server"); 
			return null;
		}
		
		ServerConnection predecessor_server = null;

		
		// We are the first server:
		if(0 == cur_server_index){
			predecessor_server = ecs_api.server_connections.get(ecs_api.server_connections.size()-1);
		}
		else{
			predecessor_server = ecs_api.server_connections.get(cur_server_index-1);
		}
		return predecessor_server;
	}
	
	
	
	/**
	 * Helper function
	 * 
	 * @param ecs_api: ECS main api
	 * @param connection: server to act on
	 * @return: first successor
	 */
	ServerConnection get_first_successor(ECSFunc ecs_api, ServerConnection connection){
		 
		// There are no successor:
		if(ecs_api.server_connections.size() < 2){
			return null;
		}
			
		// Index of current server:
		int cur_server_index = get_index_from_server_connections(ecs_api, connection);
		if(cur_server_index==-1){
			logger.error("cannot find the current server"); 
			return null;
		}
		ServerConnection successor_server = null;
		
		
		if(cur_server_index == ecs_api.server_connections.size()-1){//last server
			successor_server = ecs_api.server_connections.get(0);
		}else{
			successor_server = ecs_api.server_connections.get(cur_server_index+1);
		}
		
		return successor_server;
	}
	
	
	

	/**
	 * Helper function
	 * 
	 * @param ecs_api: ECS main api
	 * @param connection: server to act on
	 * @return: second successor
	 */
	ServerConnection get_second_successor(ECSFunc ecs_api, ServerConnection connection){
		 
		// There are no second servers:
		if(ecs_api.server_connections.size() < 3){
			return null;
		}
		
		// Index of current server:
		int cur_server_index = get_index_from_server_connections(ecs_api, connection);
		if(cur_server_index==-1){
			logger.error("cannot find the current server"); 
			return null;
		}
		
		
		
		
		
		// The second server ahead of us:
		// (Check if we have to loop around)
		//
		ServerConnection successor_server = null;

		
		if( (ecs_api.server_connections.size()-1) - cur_server_index > 1 ){
			successor_server = ecs_api.server_connections.get(cur_server_index+2);
			
		
		// We have to loop around:
		}else{
			
			int max_loop_dist = 1;
			
			//  - if we are at the end of the list --> get 1
			//  - second last element --> get 0
			int loop_distance = max_loop_dist - ((ecs_api.server_connections.size()-1) - cur_server_index);
			
			
			successor_server = ecs_api.server_connections.get(loop_distance);
		}
		
		return successor_server;
	}
	
	
	
	
	
	/**
	 * Helper function
	 * 
	 * @param ecs_api: ECS main api
	 * @param connection: server to act on
	 * @return: third successor
	 */
	ServerConnection get_third_successor(ECSFunc ecs_api, ServerConnection connection){
		 
		// There are no second servers:
		if(ecs_api.server_connections.size() < 4){
			return null;
		}
		
		// Index of current server:
		int cur_server_index = get_index_from_server_connections(ecs_api, connection);
		if(cur_server_index==-1){
			logger.error("cannot find the current server"); 
			return null;
		}
		
		
		
		
		
		// The third server ahead of us:
		// (Check if we have to loop around)
		//
		ServerConnection successor_server = null;

		
		if( (ecs_api.server_connections.size()-1) - cur_server_index > 2 ){
			successor_server = ecs_api.server_connections.get(cur_server_index+3);
			
		
			
		// We have to loop around:
		}else{
			
			int max_loop_dist = 2;
			
			//  - if we are at the end of the list --> get 2
			//  - second last element --> get 1
			//  - third last element --> get 0
			int loop_distance = max_loop_dist - ((ecs_api.server_connections.size()-1) - cur_server_index);
			
			
			successor_server = ecs_api.server_connections.get(loop_distance);
		}
		
		return successor_server;
	}
	
	
	
	
	
	/**
	 * Helper function
	 * 
	 * @param ecs_api: ECS main api
	 * @param connection: server to look for 
	 * @return: index of server
	 */
	int get_index_from_server_connections(ECSFunc ecs_api, ServerConnection connection){
		
		int connection_add_index = ecs_api.server_connections.lastIndexOf(connection);
		if(connection_add_index == -1){
			logger.error("cannot find the connection just added. This shouldn't happen");
			return -1;
		}else{
			return connection_add_index;
		}
		
	}
	
}
