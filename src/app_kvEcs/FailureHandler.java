package app_kvEcs;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import app_kvEcs.ECSFunc.ServerConnection;
import common.messages.ECSMessage;
import common.messages.MetadataEntry;
import common.messages.ECSMessage.ECS_Status;

/*
 * Provides failure handling for the FailureServer and ECSFunc.
 * 
 */
public class FailureHandler {

	// Logging events:
	static Logger logger = Logger.getRootLogger();
	
	ECSReplication replication_api = new ECSReplication();

	
	

	
	
	/**
	 * Uses a failure warning as a signal to ping all KVServers, to truly determine which 
	 * servers have failed and remove them from the server connections list.
	 * 
	 * @param ecs_api: ECS main functionality
	 * 
	 * @return: True: A failure server was detected and handled.
	 * 			False: No failed servers were detected.
	 * 
	 */
	Boolean handle_failure_warning(ECSFunc ecs_api, List<ServerEntry> available_servers){
	
		logger.info("\n\nAttempting to ping all servers....\n\n");

		List <ServerConnection> server_connections = ecs_api.get_server_connections();
		List <ServerConnection> to_remove = new ArrayList<ServerConnection>(); // Servers to be removed:
				
		
			
			
		/*
		 * Attempt to ping each server, if no response
		 * 						 ---> Handle the server failure
		 * 						 ---> Delete it from the current server connections:
		 */
		ECS_Status response = null;
		
		
		
		/*
		 * Find all servers that failed:
		 */
		for(ServerConnection cur_server : server_connections){
			
			// Ping to send:
			ECSMessage encapsulated_message = new ECSMessage(ECS_Status.PING, null, null, null, null, null, -1, null, -1);
			
			// Get response:
			response = ecs_api.forwarding_message_to_server(cur_server, encapsulated_message);

			
			/*
			 * IF we are here, we have detected a server failure:
			 */
			if(response == null || response != ECS_Status.PING_ACK){
				logger.info("\n\nFailure server is removing the following server from the list of active connections.");
				logger.error("KVServer: ("+ cur_server.metadata.hostname +","+ cur_server.metadata.port +") \n\n");
				
				to_remove.add(cur_server);
			}
		}
		
		
		
		/*
		 * Remove all failed servers:
		 */
		if(to_remove.size() == 0){
			logger.info("\n\nNo failed servers were detected after pinging all servers.");
		}
		else{
			for(ServerConnection remove_server : to_remove){
				// Handle the server failure as per document:
				handle_server_failure(available_servers, ecs_api, remove_server);
			}
		}
		
		
		// If we removed and handled at least one failed server:
		//
		if(to_remove.size() != 0)
			return true;
		else
			return false;
	}
	
	
	
	
	
	
	/**
	 * 
	 * As per Milestone 3 documentation:
	 * 
	 * 1) Maintains the replication invariant under the assumption that the failed server was removed (much like removeNode()).
	 * 2) Calls addNode()
	 * 
	 * 
	 * @param failed_server: Server confirmed to have failed.
	 */
	private void handle_server_failure(List<ServerEntry> available_servers, ECSFunc ecs_api, ServerConnection failed_server){
		
		logger.info("\n\n\nRevalidating the replication invariant...\n\n\n");

		/*
		 * Assume replication invariant was maintained before the server died.
		 * Now, we can assume the successor already has the failed server's coordinator data.
		 */
		
		
		/*
		 * Servers affected due to server removal, need to get them before removing the server from the connections list.
		 */
		ServerConnection first_predecessor = replication_api.get_first_predecessor(ecs_api, failed_server);
		ServerConnection second_predecessor = replication_api.get_second_predecessor(ecs_api, failed_server);
		ServerConnection first_successor = replication_api.get_first_successor(ecs_api, failed_server);
		ServerConnection second_successor = replication_api.get_second_successor(ecs_api, failed_server);
		ServerConnection third_successor = replication_api.get_third_successor(ecs_api, failed_server);

		// Now we can remove the server from the server connections list:
		ServerConnection sc_to_remove = ecs_api.server_connections.remove(ecs_api.get_index_from_server_connections(failed_server));

		
		
		
		
		// Recalculate and update the metadata of the storage serviceRecalculate and update the metadata of the storage service
		// (Note: Removing an element from a sorted list ---> still sorted)
		//
		ecs_api.initialize_servers_metadata();
		ecs_api.print_all_hash_info();
		
		
		
		
		// Send a metadata update to the remaining storage servers
		// Note: First we pack the list of metadata to send
		//
		List<MetadataEntry> metadata_entries = new ArrayList<MetadataEntry>();
		for (ServerConnection i_connection : ecs_api.server_connections) {
			metadata_entries.add(i_connection.metadata);
		}
		
		
		
		
		/*
		 * 
		 * MILESTONE 3:
		 * 
		 * 1) Makes sure that the replication invariant is maintained, assuming the failed server is out of the picture.
		 * 2) Also reinitializes all server metadata.
		 * 
		 */
		replication_api.single_node_remove_replicated_data(ecs_api, sc_to_remove, metadata_entries, ecs_api.failure_hostname, ecs_api.failure_port,
															first_predecessor, second_predecessor, first_successor, second_successor, third_successor);
	
	
		logger.info("\n\n\nReplication invariant is now maintained. Now adding a new KVServer...\n\n\n");
		
		// Check if we even have any available servers:
		if(available_servers.size() == 0){
			logger.info("\n\n\nThere are no available servers to replace the failed server with.\n\n\n");
			return;
		}
		
		
		/*
		 * Now, we can finally replace the server node using addNode()
		 */
		String hostname_added = available_servers.get(0).hostname; // We know addNode() will pick the first available server.
		int port_added = available_servers.get(0).port;
		ecs_api.addNode(sc_to_remove.cache_size, sc_to_remove.replacement_strategy, available_servers);
		
		
		
		
		
		/*
		 * 
		 * Start the server we added (first we need to find it...):
		 * 
		 */
		ServerConnection new_server_connection = null;
		for(ServerConnection cur_server : ecs_api.server_connections)	{
			if(cur_server.metadata.hostname.equals(hostname_added) && cur_server.metadata.port == port_added){
				new_server_connection = cur_server;
				break;
			}
		}
					
		
		
		
		
		// Create an ECS message ---> turn it in a string to pass
		// through a "TextMessage"
		//
		ECSMessage encapsulated_message = new ECSMessage(ECS_Status.START_SERVICE, null, new_server_connection.metadata.hostname, null, null, null, -1, null, -1);

		// Send the actual message:
		ECS_Status response_type = ecs_api.forwarding_message_to_server(new_server_connection, encapsulated_message);
						
			
		// Take encapsulated ECS message and convert into a usable format:
		if(response_type != null && response_type == ECS_Status.START_SERVICE_ACK){
			new_server_connection.set_service_status(true);
			logger.info("\n\nReceived a start sevice response from ("+ new_server_connection.metadata.hostname +","+ new_server_connection.metadata.port +").\n\n");
		}
			
	}
	
	
	
	
	
}
