package app_kvClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;
import java.util.Arrays;
import java.util.Arrays.*;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import common.messages.*;
import common.messages.KVMessage.StatusType;
import common.messages.TextMessage.MessageParameters;
import logger.LogSetup;
import client.KVStore;


public class KVClient {
	
	private Logger logger = Logger.getRootLogger();
	
	// CLI parameters:
	private boolean stop = false;
	private static final String PROMPT = "Client> ";
	private BufferedReader stdin;
	public KVStore server_api = null;
	
	
	// Buffer parameters:
	private final int MAX_KEY_SIZE = 20;
	private final int MAX_VALUE_SIZE = 120000;
	
 	
	
	
	

	/**
	 * 
	 * Constructor for the main CLI logic.
	 * 
	 * @param address: IP address of the server
	 * @param port: port that the server is listening on
	 * 
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public KVClient() {
		logger.info("Client started");
	}
	
	/**
	 * Initializes and starts the client connection. 
	 * Loops until the connection is closed or aborted by the client.
	 */
	public void run() {
		
		while(!stop) {
			stdin = new BufferedReader(new InputStreamReader(System.in));
			System.out.print(PROMPT);
			
			try {
				String cmdLine = stdin.readLine();
				this.handle_command(cmdLine);
			} catch (IOException e) {
				stop = true;
				printError("CLI does not respond - Application terminated ");
			}
		}
		
	}
	
	
 		

	
	
	
	
	
	
	
	/**
	 * 
	 * Takes a command and acts upon its arguments
	 * 
	 * @param cmdLine: Command entered in the CLI
	 */
	public void handle_command(String cmdLine) {
		String[] tokens = cmdLine.split("\\s+");

		// Disconnect from server and exit CLI:
		if(tokens[0].equals("quit")) {	
			stop = true;
			if(server_api != null){
				server_api.disconnect();
				System.out.println(PROMPT + "Application exit!");
				server_api = null;
			}
		
		} 
		
		
		// Connect if not connected already:
		else if (tokens[0].equals("connect")){
			if(tokens.length == 3) {
				try{

					if(server_api == null || !server_api.isRunning()){
						String serverAddress = tokens[1];
						int serverPort = Integer.parseInt(tokens[2]);
						server_api = new KVStore(serverAddress, serverPort);
						server_api.connect();
					}
					else{
						printError("The client is actively connected to the server already!");
					}
					
				} catch(NumberFormatException nfe) {
					printError("No valid address. Port must be a number!");
					logger.info("Unable to parse argument <port>", nfe);
				} catch (UnknownHostException e) {
					printError("Unknown Host!");
					logger.info("Unknown Host!", e);
				} catch (IOException e) {
					printError("Could not establish connection!");
					logger.warn("Could not establish connection!", e);
				}
				catch(Exception e){
					printError("Unknown exception occured.");	
				}
				
			} else {
				printError("Invalid number of parameters!");
			}
			
		} 
		
		// Disconnect from server, no exit
		else if(tokens[0].equals("disconnect")) {
			if(server_api != null){
				server_api.disconnect();
				server_api = null;
				logger.info("Disconnected from the server.");
			}
		}
		
		
		// Change log level
		else if(tokens[0].equals("logLevel")) {
			if(tokens.length == 2) {
				String level = setLevel(tokens[1]);
				if(level.equals(LogSetup.UNKNOWN_LEVEL)) {
					printError("No valid log level!");
					printPossibleLogLevels();
				} else {
					System.out.println(PROMPT + 
							"Log level changed to level " + level);
				}
			} else {
				printError("Invalid number of parameters!");
			}
			
		} 
		
		// Print all commands
		else if(tokens[0].equals("help")) {
			printHelp();
		} 
		
		
		
		
		
		
		// Attempt a put request:
		else if(tokens[0].equals("put")){
			
			if(server_api !=null && server_api.isRunning()){

				if(tokens.length >= 2){
					try{
						KVMessage response;
						
						if(tokens[1].length() > MAX_KEY_SIZE){
							System.out.println("The maximum key size is 20.");
							return;
						}
							
						
						// Empty put is essentially a delete	
						// 
						if(tokens.length == 2){
							response = server_api.put(tokens[1], "null");
						}
						
						// The value can be space separated so we combine the tokens:
						else{
							
							// Combine all tokens but the first two
							String []tokens_put_value = Arrays.copyOfRange(tokens, 2, tokens.length);
							int index;
							String put_value = "";
							
							for(index = 0; index < tokens_put_value.length; index++){
								if(index != tokens_put_value.length-1)
									put_value += tokens_put_value[index] + " ";
								else 
									put_value += tokens_put_value[index];
							}
	
							
							if(put_value.length() > MAX_VALUE_SIZE){
								System.out.println("The maximum value size is 120kB.");
								return;
							}
							
							response = server_api.put(tokens[1], put_value);

						}
						
						
						
						
	
						if(response == null){
							printError("The server disconnected while reading the server reponse.");	
						}
						else{
							if(response.getStatus() == StatusType.PUT_SUCCESS)
								System.out.println("Successfully inserted KV-pair <" + response.getKey() + ", "+ response.getValue() + ">.");
							else if(response.getStatus() == StatusType.PUT_ERROR)
								System.out.println("Failed to insert KV-pair <" + response.getKey() + ", "+ response.getValue() + ">.");
							else if(response.getStatus() == StatusType.DELETE_SUCCESS)
								System.out.println("Successfully deleted KV-pair with key <" + response.getKey() + ">.");
							else if(response.getStatus() == StatusType.DELETE_ERROR)
								System.out.println("The KV-pair with key <" + response.getKey() + "> does not exist, failed to delete.");
							else if(response.getStatus() == StatusType.PUT_UPDATE)
								System.out.println("Successfully updated KV-pair <" + response.getKey() + ", "+ response.getValue() + ">.");
							else if(response.getStatus() == StatusType.SERVER_STOPPED)
								System.out.println("server is stopped. The server responsible for key " + response.getKey() + ", is currently not accepting requests.");
							else if(response.getStatus() == StatusType.SERVER_WRITE_LOCK)
								System.out.println("Server is currently blocked for write requests due to reallocation of internal data.");
							else if(response.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE)
								System.out.println("server is not responsible");
							else
								System.out.println("Unrecognized server response.");
						}
						
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				else{
					printError("Invalid number of parameters!");
					printError("Usage: put <key> <value>");
				}
			}
			else{
				printError("There is currently no connection to a server. Use connect <hostname> <port> to connect.");
			}
		}
		
		
		
		
		// Attempt a get request:
		else if(tokens[0].equals("get")){
			
			if(server_api !=null && server_api.isRunning()){
	
				
				if(tokens.length == 2){
					try{
						
						// Check maximum key size:
						if(tokens[1].length() > MAX_KEY_SIZE){
							System.out.println("The maximum key size is 20.");
							return;
						}
						
						KVMessage response = null;
						
						response = server_api.get(tokens[1]);
						
						if(response == null){
							printError("The server disconnected while reading the server reponse.");	
						}
						else{
							if(response.getStatus() == StatusType.GET_SUCCESS)
								System.out.println("Successfully retrieved KV-pair <" + response.getKey() + ", "+ response.getValue() + ">.");
							else if(response.getStatus() == StatusType.GET_ERROR)
								System.out.println("The KV-pair with key <" + response.getKey() + "> does not exist.");
							else if(response.getStatus() == StatusType.SERVER_STOPPED)
								System.out.println(response.getStatus());
							else if(response.getStatus() == StatusType.SERVER_NOT_RESPONSIBLE)
								System.out.println(response.getStatus());
						}
						
					}
					catch(Exception e){
						System.out.println("Unable to send get request to server");
					}
				}
				else{
					printError("Invalid number of parameters!");
					printError("Usage: get <key>");
				}
			}
			
			
			else {
				printError("There is currently no connection to a server. Use connect <hostname> <port> to connect.");
			}
		}
		
		
		else{
			printError("Unknown command");
			printHelp();
		}
	}
	
	
	
	/**
	 * Helper function to print out all commands.
	 */
	private void printHelp() {
		StringBuilder sb = new StringBuilder();
		sb.append(PROMPT).append("ECHO CLIENT HELP (Usage):\n");
		sb.append(PROMPT);
		sb.append("::::::::::::::::::::::::::::::::");
		sb.append("::::::::::::::::::::::::::::::::\n");
		sb.append(PROMPT).append("connect <host> <port>");
		sb.append("\t establishes a connection to a server\n");
		sb.append(PROMPT).append("put <key> <value>");
		sb.append("\t Inserts a <key,value> tuple into the server's persistent storage, if possible.\n");
		sb.append(PROMPT).append("get <key>");
		sb.append("\t Retrieves a <key, value> tuple from the server if it exists already.\n");
		sb.append(PROMPT).append("disconnect");
		sb.append("\t\t\t disconnects from the server \n");
		
		sb.append(PROMPT).append("logLevel");
		sb.append("\t\t\t changes the logLevel \n");
		sb.append(PROMPT).append("\t\t\t\t ");
		sb.append("ALL | DEBUG | INFO | WARN | ERROR | FATAL | OFF \n");
		
		sb.append(PROMPT).append("quit ");
		sb.append("\t\t\t exits the program");
		System.out.println(sb.toString());
	}
	
	
	/**
	 * Helper function to print out all log levels.
	 */
	private void printPossibleLogLevels() {
		System.out.println(PROMPT 
				+ "Possible log levels are:");
		System.out.println(PROMPT 
				+ "ALL | DEBUG | INFO | WARN | ERROR | FATAL | OFF");
	}

	
	/**
	 * 	
	 * Helper function to set the logger's log level.
	 * 
	 * @param levelString: Desired log level
	 * @return: Level set
	 */
	private String setLevel(String levelString) {
		
		if(levelString.equals(Level.ALL.toString())) {
			logger.setLevel(Level.ALL);
			return Level.ALL.toString();
		} else if(levelString.equals(Level.DEBUG.toString())) {
			logger.setLevel(Level.DEBUG);
			return Level.DEBUG.toString();
		} else if(levelString.equals(Level.INFO.toString())) {
			logger.setLevel(Level.INFO);
			return Level.INFO.toString();
		} else if(levelString.equals(Level.WARN.toString())) {
			logger.setLevel(Level.WARN);
			return Level.WARN.toString();
		} else if(levelString.equals(Level.ERROR.toString())) {
			logger.setLevel(Level.ERROR);
			return Level.ERROR.toString();
		} else if(levelString.equals(Level.FATAL.toString())) {
			logger.setLevel(Level.FATAL);
			return Level.FATAL.toString();
		} else if(levelString.equals(Level.OFF.toString())) {
			logger.setLevel(Level.OFF);
			return Level.OFF.toString();
		} else {
			return LogSetup.UNKNOWN_LEVEL;
		}
	}
	

	/**
	 * Helper function to print an error.
	 * 
	 * @param error: Error message
	 */
	private void printError(String error){
		System.out.println(PROMPT + "Error! " +  error);
	}
	
	
	
	
	 /**
     * Main entry point for the echo server application. 
     */
    public static void main(String[] args) {
    	try {
			new LogSetup("logs/client.log", Level.ERROR);
			KVClient client_app = new KVClient();
			client_app.run(); // Start CLI logic
		}
    	catch (IOException e) {
			System.out.println("Error! Unable to initialize logger!");
			e.printStackTrace();
			System.exit(1);
		}
    	
    }
}
