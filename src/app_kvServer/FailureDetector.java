package app_kvServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import app_kvEcs.ECSFunc.ServerConnection;
import common.messages.TextMessage;
import common.messages.ECSMessage.ECS_Status;
import common.messages.TextMessage.MessageParameters;
import common.messages.ServerMessage.KVS_Status;
import common.messages.ECSMessage;
import common.messages.ServerMessage;


/*
 * Continuously contacts the next server in the ring until it dies.
 * (In which case it will contact the ECS giving an alert of failure).
 * 
 */
public class FailureDetector extends Thread{

	// Server we track:
	String hostname;
	int port;
	
	
	// Used to contact ECS failure server to alert of failure:
	String failure_hostname = null;
	int failure_port = -1;
	Socket failure_server_connection = null;
	
	
	// HEARTBEAT PERIOD/INTERVAL (ms):
	static final int HEARTBEAT_INTERVAL = 10000;
	
	
	
	

	// Logging errors and ECS/Server contact:
	static Logger logger = Logger.getRootLogger();

	
	// Receiver buffer/message parameters:
	private static final int BUFFER_SIZE = 1024;
	private static final int DROP_SIZE = 1024 * BUFFER_SIZE;
	public static final byte HEADER_OPEN = 0x7E;
	public static final byte HEADER_CLOSE = 0x5E;
	public static final byte MESSAGE_STUFFING = 0x23;
	private OutputStream output;
	private InputStream input;
	private Socket tracked_server = null;
	
	// API specific local variables:
	private Boolean isOpen = true;	
	private Boolean alerted_ECS = false;
	private Boolean is_tracking = true;
	
	
	
	
	/**
	 * Constructor
	 * 
	 * @param hostname: Hostname of the server we track.
	 * @param port: Port of the server we track.
	 */
	public FailureDetector(String _hostname, int _port, String _failure_hostname, int _failure_port){
		this.hostname = _hostname;
		this.port = _port;
		this.failure_hostname = _failure_hostname;
		this.failure_port = _failure_port;
	}
	
	
	
	
	
	
	/*
	 * Main execution loop:
	 */
	public void run(){
		

			// Try to get the connect response from the newly launched KVServer:
			Boolean successful_connect = false;
			try {

			
			// Attempt to connect:
			isOpen = true;
			tracked_server = new Socket(hostname, port);

			output = tracked_server.getOutputStream();
			input = tracked_server.getInputStream();
			TextMessage connect_response = receiveMessage();

				
			
			
				// On connection success:
				if(connect_response != null){
					successful_connect = true;
				}
				
			
				// Connection closes when attempting to read a message:
				else{
					logger.error("Connection closed during connect phase to the following server:\n");
					logger.error("Hostname: \"" + hostname + "\"\n");
					logger.error("Port: \"" + port + "\"\n");
				}
				
			} 
			
			// No such host exists:
			catch (UnknownHostException e){
				logger.error("Hostname was not found for the following server:\n");
				logger.error("Hostname: \"" + hostname + "\"\n");
			}
			// Trouble opening a socket:
			catch (IOException ioe) {
				logger.error("Input and output streams could not be established for the following server:");
				logger.error("Hostname: \"" + hostname + "\"\n");
				logger.error("Port: \"" + port + "\"\n");
			}
			
			// Exit early.
			if(!successful_connect){
				alert_ECS();
				logger.info("Closing the failure detector (failed to connect)...");
				return;
			}
			
			
			
			
			
			
			
			
			
			
			
			// While we hold a connection to the server we are tracking:
			//
			ServerMessage encapsulated_message = new ServerMessage(KVS_Status.HEARTBEAT);
			
			while(isOpen && is_tracking){
				
				// Wait 5 seconds in between heartbeats:
 				//
				try {
					Thread.sleep(HEARTBEAT_INTERVAL);
				} catch (InterruptedException e) {
					logger.error(e);
				}
				
				if(!is_tracking)
					break;
				
				
		
				
				KVS_Status response = null;
				try{
					
					// Create encapsulated string:
					Gson gson = new Gson();
					String encapsulated_string = gson.toJson(encapsulated_message);
					
					// Send TextMessage (Contains the HEARTBEAT):
					TextMessage message_to_send = new TextMessage(encapsulated_string, null,null,null,false, true); 
					sendMessage(message_to_send);
					

					
					
					// Wait for a response for intializing of metadata:
					TextMessage latestMsg = receiveMessage();
					isOpen = false; // Will be set to true if we get a heartbeat ACK

					
					
					
					// Take encapsulated ECS message and convert into a usable format:
					if(latestMsg != null){
						
						String encapsulated_ECS_message = latestMsg.getMsg();
						gson = new Gson();
						ServerMessage json_parameters = gson.fromJson(encapsulated_ECS_message, ServerMessage.class);
			
						// Set response type:
						response = json_parameters.get_message_type();
						logger.info("Received a response of type \""+ response +"\" from ("+ hostname +","+ port +").");
						
						if(response == KVS_Status.HEARTBEAT_ACK)
							isOpen = true;
					}
					else{
						logger.error("Received a non heartbeat ACK message.");
						logger.error("TYPE: \""+response +"\".\n\n");

					}
				}
				catch(IOException e){
					logger.error("Failed to send a message of type \""+encapsulated_message.get_message_type()+"\" to ("+ hostname +","+ port +").");
				}
				
				
			}
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			// Tell the server connection we made that we are no longer tracking ---> they should also close:
			//
			if(!is_tracking){
				
				KVS_Status response = null;
				try{
					
					
					encapsulated_message = new ServerMessage(KVS_Status.DONE_TRACKING);

					
					// Create encapsulated string:
					Gson gson = new Gson();
					String encapsulated_string = gson.toJson(encapsulated_message);
					
					// Send TextMessage (Contains the HEARTBEAT):
					TextMessage message_to_send = new TextMessage(encapsulated_string, null,null,null,false, true); 
					sendMessage(message_to_send);
					

					
					
					// Wait for a response for finishing tracking:
					TextMessage latestMsg = receiveMessage();
					
					// Take encapsulated ECS message and convert into a usable format:
					if(latestMsg != null){
						
						String encapsulated_ECS_message = latestMsg.getMsg();
						gson = new Gson();
						ServerMessage json_parameters = gson.fromJson(encapsulated_ECS_message, ServerMessage.class);
			
						// Set response type:
						response = json_parameters.get_message_type();
						logger.info("Received a response of type \""+ response +"\" from ("+ hostname +","+ port +").");
						
						if(response == KVS_Status.DONE_TRACKING_ACK)
							logger.info("Successfully finished tracking.");
					}

				}
				catch(IOException e){
					logger.error("Failed to send a message of type \""+encapsulated_message.get_message_type()+"\" to ("+ hostname +","+ port +").");
				}
				
			}
				
				
			else{	// Alert the ECS if we have not already (only if we are tracking):
				alert_ECS();
			}
			
			logger.info("Closing the failure detector...");

	}

		
	
	
	
	
	
	
	
	
	/**
	 * Tells this FailureDetector thread to quit.
	 */
	public void stop_tracking(){
		this.is_tracking = false;
	}
	
	
	
	/**
	 * Creates a connection to the ECS in order to alert of a failure.
	 */
	private void alert_ECS(){
		
		// No sense in alerting again...
		if(alerted_ECS)
			return;
		alerted_ECS = true;
	
		logger.error("Failure detector detected failure of server (" + hostname + "," + port +  ")");	
		logger.info("Alerting the ECS....(" + failure_hostname + "," + failure_port + ")");
		
		
		
		
		
		/*
		 * Connect to ECS failure server and send alert message:
		 */
		try{
			this.failure_server_connection = new Socket(failure_hostname, failure_port);
			

			output = this.failure_server_connection.getOutputStream();
			input = this.failure_server_connection.getInputStream();
			isOpen = true;


			/*
			 * Try to read connection message:
			 */
			TextMessage connect_response = null;
			try{
				connect_response = receiveMessage();
			}
			catch(Exception e){
				logger.error(e);
			}
			

			
			
			// On connection success:
			if(connect_response != null){
				
				
				/*
				 * Send a failure warning:
				 */
				ECSMessage encapsulated_message = new ECSMessage(ECS_Status.FAILURE_WARNING, null, null, null, null, null, -1, null, -1);
				ECS_Status response = null;
				
				
				// Create encapsulated string:
				Gson gson = new Gson();
				String encapsulated_string = gson.toJson(encapsulated_message);
				
				// Send TextMessage:
				TextMessage message_to_send = new TextMessage(encapsulated_string, null,null,null,true, false); 
				sendMessage(message_to_send);
				
				// Wait for a response for intializing of metadata:
				TextMessage latestMsg = receiveMessage();
				
				
				// Take encapsulated ECS message and convert into a usable format:
				if(latestMsg != null){
					
					String encapsulated_ECS_message = latestMsg.getMsg();
					gson = new Gson();
					ECSMessage json_parameters = gson.fromJson(encapsulated_ECS_message, ECSMessage.class);
		
					// Set response type:
					response = json_parameters.get_message_type();
					logger.info("Received a response of type \""+ response +"\" from ("+ failure_hostname +","+failure_port +").");
				}
				else{
					logger.info("The failure server disconnected.");
				}
					
			}
			
		
			// Connection closes when attempting to read a message:
			else{
				logger.error("Connection closed during connect phase to the following server:\n");
				logger.error("Hostname: \"" + failure_hostname + "\"\n");
				logger.error("Port: \"" + failure_port + "\"\n");
			}
			
			
			failure_server_connection.close();
		}
		catch(UnknownHostException e){
			logger.error("The given hostname" + failure_hostname + " cannot be connected to.");
		}
		catch(IOException ioe){
			logger.error("Failed to make a socket for the given (hostname,port): ("+ failure_hostname+ ","+ failure_port+ ")");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Method sends a TextMessage using this socket.
	 * @param msg the message that is to be sent.
	 * @throws IOException some I/O error regarding the output stream 
	 */
	public void sendMessage(TextMessage msg) throws IOException {
		byte[] msgBytes = msg.getMsgBytes(); 
		String string_format = new String(msgBytes);
		output.write(msgBytes, 0, msgBytes.length);
		output.flush();
		logger.info("Send message:\t '" + string_format + "'");
    }

	
	
	

	/**
	 * Attempts to wait and read a request from a client.
	 * 
	 * @return: TextMessage - formatted message received 
	 * 			NULL - indicating a closed connection
	 * 
	 * @throws IOException: If there is an external failure reading from the socket
	 */
	private TextMessage receiveMessage() throws IOException {
		
		// Message to return is initially null to indicate failure reading bytes:
		TextMessage msg = null;
		
		
		int index = 0;
		byte[] msgBytes = null, tmp = null;
		byte[] bufferBytes = new byte[BUFFER_SIZE];
		byte cur_byte = 0x0;
		byte prev = 0x0;
		
		/* read first char from stream */
		boolean reading = true;


		// While we haven't read the opening header for a message, keep reading before we record anything
		//
		while(!(cur_byte == HEADER_OPEN && prev != MESSAGE_STUFFING) && reading){
			prev = cur_byte;
			cur_byte = (byte)input.read();
			

			if(cur_byte == -1){
				reading = false;
				isOpen = false;
			}
		}
		
		// Error while reading, ie client close:
		if(!isOpen)
			return msg;
		
		
		
		/* read next char from stream */
		cur_byte = (byte) input.read();
		
				
		
		
		// While we haven't read the closing header for the message, record everything, this is the JSON object
		//
		boolean stuff_last_char = false;
		
		while(!(cur_byte == HEADER_CLOSE &&  prev!= MESSAGE_STUFFING) && reading) {
			
			
			// IF we enter and we previously skipped writing because of a stuff
			// The stuff is an actual character within the message:
			//
			if(stuff_last_char){

				// This was not an attempt to stuff a header open either:
				//
				if(cur_byte != HEADER_OPEN){
					bufferBytes[index] = cur_byte;
					prev = cur_byte;
							
					index++;
				}
			}
			
			
			
			// Update whether the current character is a stuff character
			if(cur_byte == MESSAGE_STUFFING)
				stuff_last_char = true;
			else
				stuff_last_char = false;

				
			
			
			if(!stuff_last_char){
					
				/* CR, LF, error */
				/* if buffer filled, copy to msg array */
				if(index == BUFFER_SIZE) {
					if(msgBytes == null){
						tmp = new byte[BUFFER_SIZE];
						System.arraycopy(bufferBytes, 0, tmp, 0, BUFFER_SIZE);
					} else {
						tmp = new byte[msgBytes.length + BUFFER_SIZE];
						System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
						System.arraycopy(bufferBytes, 0, tmp, msgBytes.length,
								BUFFER_SIZE);
					}
	
					msgBytes = tmp;
					bufferBytes = new byte[BUFFER_SIZE];
					index = 0;
				} 
				
				/* only read valid characters, i.e. letters and constants */
				bufferBytes[index] = cur_byte;						
				index++;
				
				/* stop reading is DROP_SIZE is reached */
				if(msgBytes != null && msgBytes.length + index >= DROP_SIZE) {
					reading = false;
				}
			
			}
			
			prev = cur_byte;
			/* read next char from stream */
			cur_byte = (byte) input.read();
			
			if(cur_byte == -1){
				reading = false;
				isOpen = false;
			}
				
		}
		
		// Error reading bytes, ie client close:
		if(!isOpen)
			return msg;
		
		
		
		if(msgBytes == null){
			tmp = new byte[index];
			System.arraycopy(bufferBytes, 0, tmp, 0, index);
		} else {
			tmp = new byte[msgBytes.length + index];
			System.arraycopy(msgBytes, 0, tmp, 0, msgBytes.length);
			System.arraycopy(bufferBytes, 0, tmp, msgBytes.length, index);
		}
		
		
		// MsgBytes contains actual message which is a JSON string
		//
		// We convert the JSON string into a MessageParameters object for convenience
		//
		msgBytes = tmp;
		String string_format = new String(msgBytes);
		
		Gson gson = new Gson();
		MessageParameters json_parameters = gson.fromJson(string_format, MessageParameters.class);
		
		
		/* build final String */
		msg = new TextMessage(json_parameters.get_msg(), json_parameters.get_key(), json_parameters.get_value(), json_parameters.get_status(), json_parameters.get_isECS(), json_parameters.get_isServer());
		
		
		/*
		 * Depends on who we're talking to currently:
		 */
		if(failure_server_connection != null){
			logger.info("RECEIVE \t<" 
					+ failure_server_connection.getInetAddress().getHostAddress() + ":" 
					+ failure_server_connection.getPort() + ">: '" 
					+ string_format + "'");
		}
			
		else{
			logger.info("RECEIVE \t<" 
					+ tracked_server.getInetAddress().getHostAddress() + ":" 
					+ tracked_server.getPort() + ">: '" 
					+ string_format + "'");
		}
				
		return msg;		

    }
	
}
