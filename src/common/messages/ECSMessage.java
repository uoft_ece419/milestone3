package common.messages;

import java.util.List;

/**
 * 
 * Used specifically for ECS to KVServer communication ---> encapsulated in TextMessage "msg" parameter
 *
 */
public class ECSMessage {

	// For initializing of metadata  (Hostname of receiver server)
	String receiver_hostname;
	
	// Used for movedata(server, range)
	String move_destination;
	int move_port;
	byte[] start_range;
	byte[] end_range;
	
	// Used to let KVservers know of the failure server:
	String failure_hostname;
	int failure_port;
	
	
	

	/*
	 * 
	 * Possible messages types sent between ECS and KVServer
	 */
	public enum ECS_Status{
		
		SHUTDOWN,						// All KVServers are being shutdown
		
		SHUTDOWN_SOFT,
		SHUTDOWN_HARD, // (Also deletes all entries)
		
		SHUTDOWN_ACK,
		
		INITIALIZE_METADATA,			// Initialization of meta data for consistent hashing
		INITIALIZE_ACK,
		
		START_SERVICE,					// Setting whether a given server should fulfill client requests
		START_SERVICE_ACK,
		STOP_SERVICE,
		STOP_SERVICE_ACK,
		
		WRITE_LOCK,						// To indicate that the server is currently moving data
		WRITE_UNLOCK,
		WRITE_LOCK_ACK,
		WRITE_UNLOCK_ACK,
		
		
		MOVE_DATA,						// Indicates this message is a request to move data or an ACK
		MOVE_DATA_SUCCESS,
		MOVE_DATA_FAILURE,
		
		
		FAILURE_WARNING,				// Purely for failure detection
		FAILURE_WARNING_ACK,
		
		PING,							// To determine KV_Server liveness
		PING_ACK
	}
	ECS_Status message_type;
	private List<MetadataEntry> metadata;
	
	
	
	
	
	
	
	
	/**
	 * Constructor
	 * 
	 * @param _message_type: what kind of message is being sent
	 */
	public ECSMessage(ECS_Status _message_type, List<MetadataEntry> _metadata, String _receiver_hostname, String _move_destination, byte[] _start_range, byte[] _end_range, int _move_port, String _failure_hostname, int _failure_port){
		this.message_type = _message_type;
		this.metadata = _metadata;
		this.receiver_hostname = _receiver_hostname;
		this.move_destination = _move_destination;
		this.start_range = _start_range;
		this.end_range = _end_range;
		this.move_port = _move_port;
		this.failure_hostname = _failure_hostname;
		this.failure_port = _failure_port;
	}
	
	
	
	
	
	// Accessor functions
	public ECS_Status get_message_type(){
		return this.message_type;
	}
	public List<MetadataEntry> get_metadata(){
		return this.metadata;
	}
	public String get_hostname(){
		return this.receiver_hostname;
	}
	public String get_move_destination(){
		return this.move_destination;
	}
	public byte[] get_start_range(){
		return this.start_range;
	}
	public byte[] get_end_range(){
		return this.end_range;
	}
	public int get_port(){
		return this.move_port;
	}
	public String get_failure_hostname(){
		return this.failure_hostname;
	}
	public int get_failure_port(){
		return this.failure_port;
	}
}
