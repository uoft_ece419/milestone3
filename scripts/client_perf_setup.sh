#!bin/bash

num_servers=10
num_repeat=5

ECS_Jar_path=/nfs/ug/homes-0/s/shiterry/ECE419/test/milestone3/ms3-ecs.jar
SERVER_Jar_path=/nfs/ug/homes-0/s/shiterry/ECE419/test/milestone3/ms3-server.jar


mkdir testingFiles
cp $ECS_Jar_path testingFiles

echo "ug151.eecg.utoronto.ca 42001" >> testingFiles/master_server.config

for i in `seq 1 $num_servers`;
do
	server_jar_folder="testingFiles/Server$i"
	mkdir $server_jar_folder
	cp $SERVER_Jar_path $server_jar_folder
	echo "KVServer ug$((150+$i)).eecg.utoronto.ca $((42000 + $i)) ~/ECE419/test/milestone3/scripts/testingFiles/Server$i/ms3-server.jar" >> testingFiles/perf_server.config
done

echo "FailureServer ug151@eecg.utoronto.ca 40600" >> testingFiles/perf_server.config
