#!/bin/bash

base_url="ugNUM.eecg.utoronto.ca"
num_machine=5
num_threads_per_machine=5
num_operations_per_thread=20
starting_machine=150
file_path=/nfs/ug/homes-0/s/shiterry/ECE419/test/milestone3/scripts/enron_emails.txt
host_address=ug151.eecg.utoronto.ca
port_num=42001

num_machines=$(($starting_machine + $num_machine -1))

for i in `seq $starting_machine $num_machines`;
do
	final_url=${base_url/NUM/$i}
	
	starting_line=$((($i-$starting_machine)*$num_threads_per_machine*num_operations_per_thread))
	echo "$final_url $starting_line"
	ssh -o StrictHostKeyChecking=no $final_url "cd ~/ECE419/test/milestone3; java -jar ms3-perf.jar $num_threads_per_machine $num_operations_per_thread $starting_line $file_path $host_address $port_num |grep Thread" &

done
exit 0
