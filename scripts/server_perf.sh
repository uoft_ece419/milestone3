#!bin/bash

base_path=/nfs/ug/homes-0/s/shiterry/ECE419/test/milestone3
jar_path=~/ECE419/test/milestone3/scripts/testingFiles

num_servers=10
cache_size=1024
cache_type=FIFO
num_repeat=5
num_KV_pairs=50000

ECS_Jar_path=$base_path/ms3-ecs.jar
SERVER_Jar_path=$base_path/ms3-server.jar
SERVER_PERF_Jar_path=$base_path/ms3-server-perf.jar

echo "Scalability Performance Test: servers: $num_servers repeat:$num_repeat storage_size:$num_KV_pairs"

echo "Creating testing folder/files"

mkdir testingFiles
cp $ECS_Jar_path testingFiles
cp $SERVER_PERF_Jar_path testingFiles

echo "ug151.eecg.utoronto.ca 40201" >> testingFiles/master_server.config

for i in `seq 1 $num_servers`;
do
	server_jar_folder="testingFiles/Server$i"
	mkdir $server_jar_folder
	cp $SERVER_Jar_path $server_jar_folder
	echo "KVServer ug$((150+$i)).eecg.utoronto.ca $((40200 + $i)) $jar_path/Server$i/ms3-server.jar" >> testingFiles/perf_server.config

done
echo "FailureServer 127.0.0.1 55555" >> testingFiles/perf_server.config


echo "Starting Performance test..."
cd testingFiles
java -jar ms3-server-perf.jar perf_server.config $num_servers $cache_size $num_repeat $cache_type ug151.eecg.utoronto.ca 40201

echo "Tests complete. Removing files..."
rm -rf testingFiles
